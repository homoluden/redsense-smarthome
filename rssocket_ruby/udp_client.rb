require 'socket'
require_relative 'protocol'

# Connection params
$port = 8899
$host = 'localhost'

def make_request(cmd, a,b)
    add_request = CustomProtocol.new
    add_request.command_word = cmd
    # add_request.op1 = a
    # add_request.op2 = b
    add_request
end

def print_answer(sock)
    msg, sender = sock.recvfrom(20) # reads up to 20 bytes 
    puts "server responded: #{msg} " 
end

def send_request(sock, data)
    sock.send(data, 0, $host, $port)
end


# -- MAIN --
# open the connection
s = UDPSocket.new

# make an add request and send it out over the socket
# print the answer by reading the result off the socket
req1 = make_request("ADD",5,4)
puts "sending add request over socket ..."
send_request(s, req1.to_binary_s)
print_answer(s)

# ok, now try a multiply request
req2 = make_request("MULT",6,7)
puts "sending multiply request over socket ..."
send_request(s, req2.to_binary_s)
print_answer(s)


# ok, now try a REGME request
req2 = make_request("REG:0xFFFFFFFF-0xFFFFFFFF-0xFFFFFFFF#",0,0)
puts "sending regme request over socket ..."
send_request(s, req2.to_binary_s)
print_answer(s)



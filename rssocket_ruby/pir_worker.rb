require 'thread'

class PirWorker
	@enabled = false
  @poll_delay = 1

	attr_accessor :udp_src

	def initialize(udp_src)
		@udp_src = udp_src
	end
	
	def request_pir
		@udp_src.reply "GET:PIR#" if @udp_src
	end
	
	def run
		@enabled = true if @udp_src
		
		@thread = Thread.new do
		   loop do
			  break if !@enabled

        puts "PirWorker.run > PIR requested"
			  request_pir

			  sleep 5
		   end
    end

    nil
	end

	def stop
		@enabled = false
	end
	
end

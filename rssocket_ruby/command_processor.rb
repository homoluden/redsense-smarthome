require 'thread'
require 'socket'
require 'net/http'

require_relative 'pir_worker'

class CommandProcessor
  @sensors = {}

  def initialize
    @sensors = {}
  end

  def restart_pir_loop (worker)
    worker.stop
    sleep 3
    worker.run
  end

  def parse(msg, msg_src)
    ip = msg_src.remote_address.inspect_sockaddr

    # Example request: REG:0672FF55-51538850-87123432#
    match = msg.match(/(\w{0,4}?):([\w-]*)#/)
    cmd,param = match[1].downcase,match[2] if match

    puts "CommandProcessor > parse : Message \"#{msg}\" has wrong format!" unless match
    return unless match

    if cmd == "reg"
      reg param, msg_src
    elsif cmd == "pir"
      send_pir param, msg_src
    else
      puts "Unknown command received from #{ip}!",msg
    end
  end

  def reg(uid, msg_src)

    pir_worker = PirWorker.new(msg_src)

    ip = msg_src.remote_address.inspect_sockaddr
    @sensors[ip] = {:uid => uid, :pir_worker => pir_worker}

    puts "CmdProc > Registered : #{ip} => #{uid}"

    msg_src.reply "OK:REG#"

    sleep 3

    pir_worker.run
  end

  def send_pir(val, msg_src)
    ip = msg_src.remote_address.inspect_sockaddr
    uid = @sensors[ip][:uid]
    puts "#{val}  @  IP: #{ip} (#{uid})"

    uri = URI('http://localhost:3000/records/append')
    res = Net::HTTP.post_form(uri, 'uid' => uid, 'txt' => "PIR:#{val}")
    puts res.body
  end
end
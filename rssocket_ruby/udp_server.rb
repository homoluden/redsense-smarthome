require 'thread'
require 'socket'
require 'net/http'

require_relative 'command_processor'

cmd_proc = CommandProcessor.new
port = 8899
puts "Started UDP server on #{port}..."

Socket.udp_server_loop(port) { |msg, msg_src|
    cmd_proc.parse msg, msg_src
}
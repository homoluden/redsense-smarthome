//= require angular-ui-router
angular.module('sensorsApp', ['ui.router'])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('dash', {
                    url: '/dash',
                    templateUrl: '/dash.html',
                    controller: 'DashCtrl'
                })
                .state('pir', {
                    url: '/pir',
                    templateUrl: '/pir.html',
                    controller: 'PirCtrl'
                })
                .state('dht', {
                    url: '/dht',
                    templateUrl: '/dht.html',
                    controller: 'DhtCtrl'
                })
                .state('flood', {
                    url: '/flood',
                    templateUrl: '/flood.html',
                    controller: 'FloodCtrl'
                })
                .state('add-device', {
                    url: '/add-device',
                    templateUrl: '/add-device.html',
                    controller: 'AddDeviceCtrl'
                });

            $urlRouterProvider.otherwise('dash');
        }])
    .controller('DashCtrl', [
        '$scope', '$http', '$interval',
        function($scope, $http, $interval){
            $scope.pirs = [
                /*{
                    uid: '0672FF55-51538850-87123432',
                    label: 'ЗАЛ',
                    state: false
                },
                {
                    uid: 'C82EFF55-51538850-87123432',
                    label: 'СПЛ',
                    state: false
                },
                {
                    uid: 'B109FF55-51538850-87123432',
                    label: 'КОР',
                    state: true
                },
                {
                    uid: 'F321FF55-51538850-87123432',
                    label: 'ДЕТ',
                    state: false
                }*/
            ];

            $http.get('/sensors.json').success(function(data){
                $scope.pirs.length = 0;
                data.forEach(function (s){

                    $scope.pirs.push(s);

                    /*$http.get("/records/tail/" + s.id + "/20.json").success(function (response){
                        console.log(response);

                        if(response.status == 'ok')
                        {
                            s.records_tail = response.records;
                        }
                        else
                        {
                            console.log("Unable to get Records Tail!");
                            console.log(response.err);
                        }
                    })*/
                });

                /*console.log('Received sensors:\n');
                console.log(data);*/
            });

            $scope.stopPoll = function() {
                if (angular.isDefined(pir_poll)) {
                    $interval.cancel(pir_poll);
                    pir_poll = undefined;
                }
            };

            pir_poll = $interval(function (){

                $scope.pirs.forEach(function (s){

                    $http.get("/records/tail/" + s.id + "/20.json").success(function (response){
                        //console.log(response);

                        if(response.status == 'ok')
                        {
                            s.records_tail = response.records;
                        }
                        else
                        {
                            console.log("Unable to get Records Tail!\nSensor ID: " + s.id);
                            console.log(response.err);
                        }
                    })
                });
            }, 10000);

            $scope.$on('$destroy', function() {
                $scope.stopPoll();
            });
        }])
    .controller('PirCtrl', [
        '$scope',
        function($scope){

        }])
    .controller('DhtCtrl', [
        '$scope',
        function($scope){

        }])
    .controller('FloodCtrl', [
        '$scope',
        function($scope){

        }])
    .controller('AddDeviceCtrl', [
        '$scope',
        function($scope){

        }]);
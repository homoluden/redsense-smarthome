class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.datetime :time
      t.string :message
      t.string :sensor_id

      t.timestamps null: false
    end
  end
end

#ifndef __STATE_MACHINE__
#define __STATE_MACHINE__

enum sm_error
{
	SM_ERR_OK = 0,			// Success
	SM_ERR_TIMEOUT = -1,	// Time is out. Response wasn't received in time.
	SM_ERR_DATA = -2,		// Data issue. Wrong argument or wrong data.
	SM_ERR_RESP = -4,		// Bad response. Network issue of H/W Interface failure.
	SM_ERR_BAD_CMD = -8,	// Bad Command. Unknown command requested or bad command parameters.
	
	SM_ERR_NOTIMPL = -64,	// Not implemented.
	SM_ERR_UNKNOWN = -128	// Unknown issue. Unexpected return from lib methods or other unexpected error.
};

enum states { SM_STARTUP, SM_REGISTERING, SM_SHOWING_HELP, SM_AWAITING_CMD, SM_EXECUTING_CMD, SM_MAX_STATES} current_state;
enum events { SM_PROCEEDING, SM_SWITCHING, SM_MAX_EVENTS } new_event;

int action_startup_proceed(void);
int action_startup_switch(void);
int action_registering_proceed(void);
int action_registering_switch(void);
int action_showing_help_proceed(void);
int action_showing_help_switch(void);
int action_awaiting_cmd_proceed(void);
int action_awaiting_cmd_switch(void);
int action_executing_cmd_proceed(void);
int action_executing_cmd_switch(void);
int action_idle(void);

extern int(*const state_table[SM_MAX_STATES][SM_MAX_EVENTS])(void);

#endif // !__STATE_MACHINE__


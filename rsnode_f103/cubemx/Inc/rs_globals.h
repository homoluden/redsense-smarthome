#ifndef __RS_GLOBALS__
#define __RS_GLOBALS__

#include "stm32f1xx_hal.h"

#define BUF_LEN 100
#define CMD_LEN 3

#define CMD_RETRY 10

extern UART_HandleTypeDef huart1;

extern unsigned long *Unique;

extern uint8_t tx_buf[BUF_LEN];

extern uint16_t read_pos;
extern uint8_t rx_buf[BUF_LEN];

extern uint8_t pir_off[10]; // 9 chars
extern uint8_t pir_on[10];   // 9 chars

extern uint8_t cmd_req[CMD_LEN + 1];

extern uint8_t pir_state;		// Motion Sensor State: 0 - no motion; 1 - alarm, intruders! 

#endif // !__RS_GLOBALS__

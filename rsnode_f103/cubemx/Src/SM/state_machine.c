
#include <stdio.h>
#include <string.h>
#include "cmsis_os.h"
#include "rs_globals.h"
#include "SM/state_machine.h"

int(*const state_table[SM_MAX_STATES][SM_MAX_EVENTS])(void) = {

	{ action_startup_proceed, action_startup_switch }, /* procedures for STARTUP */
	{ action_registering_proceed, action_registering_switch }, /* procedures for STARTUP */
	{ action_showing_help_proceed, action_showing_help_switch }, /* procedures for STARTUP */
	{ action_awaiting_cmd_proceed, action_awaiting_cmd_switch }, /* procedures for STARTUP */
	{ action_executing_cmd_proceed, action_executing_cmd_switch }, /* procedures for STARTUP */
		
};

int match(char *a, char *b);

int action_startup_proceed(void)
{
	// Nothing to do at start up
	new_event = SM_SWITCHING;
	
	return SM_ERR_OK;
}	
	
int action_startup_switch(void)
{
	// Just switch to 
	current_state = SM_REGISTERING;
	new_event = SM_PROCEEDING;
	
	return SM_ERR_OK;
}

int action_registering_proceed(void)
{
	int tx_len = 0;
	int try_num = 0;
	HAL_StatusTypeDef status;
	uint8_t *cmd;
	
	 // Sending register request
	tx_len = sprintf(tx_buf, "REG:%08X-%08X-%08X#\0", Unique[0], Unique[1], Unique[2]);
	
	// Trying to send until get HAL_OK
	while (HAL_UART_Transmit(&huart1, tx_buf, tx_len, 1000) != HAL_OK)
	{
		osDelay(10);
	}
	
	osDelay(100);
	
	// Waiting the TX to complete
	while (huart1.State != HAL_UART_STATE_RESET && huart1.State != HAL_UART_STATE_READY)
	{
		osDelay(10);
	}
	
	osDelay(10);
	
	// Waiting for response
	
	memset(rx_buf, 0, BUF_LEN);
	
	do
	{
		retry: status = HAL_UART_Receive(&huart1, rx_buf, 7, 1000);
		if (status == HAL_TIMEOUT)
		{
			// FIXME: Log err here to DEBUG console
			osDelay(10);
			
			if (try_num++ > CMD_RETRY)
			{
				// Returning with TIMEOUT error
				return SM_ERR_TIMEOUT;
			}
			else
			{
				goto retry;
			}
		}
		else if (status == HAL_ERROR)
		{
			return SM_ERR_DATA;
		}
		else if (status == HAL_BUSY)
		{
			osDelay(10);
			goto retry;
		}
		else if (status != HAL_OK)
		{
			// Unknown status => Unhandled error
			return SM_ERR_UNKNOWN;
		}

		// HAL_OK
	} while (match(rx_buf, "OK:") != 0);
	
	// Parsing response
	// Checking if server answered with the same Unique ID
	if (match(rx_buf + 3, "REG") != 0)
	{
		return SM_ERR_RESP;
	}
	
	new_event = SM_SWITCHING;
	return SM_ERR_OK;
}

int action_registering_switch(void)
{
	current_state = SM_AWAITING_CMD;
	new_event = SM_PROCEEDING;
	
	return SM_ERR_OK;
}

int action_showing_help_proceed(void)
{
	new_event = SM_SWITCHING;
	return SM_ERR_NOTIMPL;
}

int action_showing_help_switch(void)
{
	current_state = SM_AWAITING_CMD;
	new_event = SM_PROCEEDING;
	
	return SM_ERR_OK;
}

int action_awaiting_cmd_proceed(void)
{
	HAL_StatusTypeDef status;
	
	memset(rx_buf, 0, BUF_LEN);
	
	do
	{
		status = HAL_UART_Receive(&huart1, rx_buf, 7, 1000);
		if (status == HAL_TIMEOUT || status == HAL_BUSY)
		{
			// No command. Skipping.
			return SM_ERR_OK;
		}
		else if (status == HAL_ERROR)
		{
			return SM_ERR_DATA;
		}
		else if (status != HAL_OK)
		{
			// Unknown status => Unhandled error
			return SM_ERR_UNKNOWN;
		}
	} while (status != HAL_OK);
	
	new_event = SM_SWITCHING;
	return SM_ERR_OK;
}

int action_awaiting_cmd_switch(void)
{
	current_state = SM_EXECUTING_CMD;
	new_event = SM_PROCEEDING;
	return SM_ERR_OK;
}

int action_executing_cmd_proceed(void)
{
	enum sm_error err = SM_ERR_BAD_CMD;
	uint8_t *cmd_pos = rx_buf;
	if (match(cmd_pos, "GET:") == 0)
	{
		// Parse GET CMD
		// FIXME: Implement whitespaces trimming
		cmd_pos += 4;
		
		// FIXME: extract to separate Commands File (commands.c(.h))
		if (match(cmd_pos, "PIR") == 0)
		{
			err = SM_ERR_OK; // Known Command
			
			if (pir_state == GPIO_PIN_SET)
			{
				// Trying to send until get HAL_OK
				while (HAL_UART_Transmit_IT(&huart1, pir_on, 7) != HAL_OK)
				{
					osDelay(1);
				}
			}
			else
			{
				// Trying to send until get HAL_OK
				while (HAL_UART_Transmit_IT(&huart1, pir_off, 8) != HAL_OK)
				{
					osDelay(1);
				}
			}
		}
	}
	
	new_event = SM_SWITCHING;
	return err;
}
int action_executing_cmd_switch(void)
{
	current_state = SM_AWAITING_CMD;
	new_event = SM_PROCEEDING;
	
	return SM_ERR_OK;
}

int action_idle(void)
{
	return SM_ERR_OK;
}

int match(char *a, char *b)
{
	int c;
	int position = 0;
	char *x, *y;
 
	x = a;
	y = b;
 
	while (*a)
	{
		while (*x == *y)
		{
			x++;
			y++;
			if (*x == '\0' || *y == '\0')
				break;         
		}   
		if (*y == '\0')
			break;
 
		a++;
		position++;
		x = a;
		y = b;
	}
	if (*a)
		return position;
	else   
		return -1;   
}
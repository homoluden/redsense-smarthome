#include "rs_globals.h"

UART_HandleTypeDef huart1;

unsigned long *Unique = (unsigned long *)0x1FFFF7E8;

uint8_t tx_buf[BUF_LEN] = { 0, };

uint16_t read_pos = 0;
uint8_t rx_buf[BUF_LEN] = { 0, };

uint8_t pir_off[10] = "PIR:OFF#"; // 8 chars
uint8_t pir_on[10] = "PIR:ON#";   // 7 chars

uint8_t cmd_req[CMD_LEN + 1] = { 0, };


uint8_t pir_state = 0x00;		// Motion Sensor State: 0 - no motion; 1 - alarm, intruders! 